<?php

/**
 * Fired during plugin activation
 *
 * @link        
 * @since      1.0.0
 *
 * @package    My_Custom_Plugin
 * @subpackage My_Custom_Plugin/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    My_Custom_Plugin
 * @subpackage My_Custom_Plugin/includes
 * @author     Santiago Bazan <sbwebdevservices@gmail.com>
 */
class My_Custom_Plugin_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
