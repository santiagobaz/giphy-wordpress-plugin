<?php

/**
 * Fired during plugin deactivation
 *
 * @link        
 * @since      1.0.0
 *
 * @package    My_Custom_Plugin
 * @subpackage My_Custom_Plugin/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    My_Custom_Plugin
 * @subpackage My_Custom_Plugin/includes
 * @author     Santiago Bazan <sbwebdevservices@gmail.com>
 */
class My_Custom_Plugin_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
